extern crate sdl2;
mod game;

fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem.window(
        "rust-sdl2 demo",
        game::WINDOW_DIMS.0 as u32,
        game::WINDOW_DIMS.1 as u32
    )
    .position_centered()
    // .opengl()
    .build()
    .unwrap();

    let mut canvas = window.into_canvas().build().unwrap();
    let mut game = game::Rogue::new(sdl_context, &mut canvas);

    game.run();
}
