use sdl2::render::{Canvas};
use sdl2::video::{Window};
use sdl2::pixels::Color;
use sdl2::rect::{Rect};
use super::{Entity};
use super::config;

pub struct Player {
    pub x: i32,
    pub y: i32,
    pub w: i32,
    pub h: i32,
    pub pos: (i32, i32),
}

impl Player {
    pub fn new(x: i32, y: i32, w: i32, h: i32, pos: (i32, i32)) -> Player {
        Player {
            x, y, w, h, pos
        }
    }
}
 
impl Entity for Player {
    fn before_update(&mut self, _canvas: &mut Canvas<Window>, _events: &Vec<sdl2::event::Event>) {}

    fn update(&mut self, canvas: &mut Canvas<Window>, events: &Vec<sdl2::event::Event>) {
        for event in events {
            match event {
                sdl2::event::Event::KeyDown { keycode: Some(sdl2::keyboard::Keycode::K), ..} => {
                    println!("keydown k from square");
                },
                _ => {}
            }
        }

        let (x, y) = self.pos;
        let (cell_w, cell_h)  = config::CELL_SIZE;
        let rect = Rect::new(
            x * cell_w,
            y * cell_h,
            self.w as u32 * cell_w as u32,
            self.h as u32 * cell_h as u32
        );

        if x + 1 > 78 {
            self.pos = (1, y + 1);
        } else {
            self.pos = (x + 1, y);
        }

        canvas.set_draw_color(Color::RGB(255, 255, 0));
        canvas.fill_rect(rect).unwrap();
        canvas.draw_rect(rect).unwrap();
    }

    fn after_update(&mut self, _canvas: &mut Canvas<Window>, _events: &Vec<sdl2::event::Event>) {}
}
