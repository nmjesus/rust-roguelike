extern crate sdl2;

use sdl2::video::{Window};
use sdl2::render::{Canvas};
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use std::time::Duration;

use super::{Player, Mob, Entity, Map};
use super::config;

pub struct Rogue<'a> {
    canvas: &'a mut Canvas<Window>,
    sdl_context: sdl2::Sdl,
    entities: Vec<Box<dyn Entity>>,
    map: Map,
}

impl<'a> Rogue<'a> {
    pub fn new(sdl_context: sdl2::Sdl, canvas: &'a mut Canvas<Window>) -> Rogue {
        let mut game = Rogue {
            canvas,
            sdl_context,
            entities: vec![
                Box::new(Player::new(0, 0, 1, 1, (10, 10))),
                Box::new(Mob::new(0, 0, 1, 1, (20, 20))),
            ],
            map: Map::new(config::GRID.0, config::GRID.1),
        };
        game.map.generate();

        game
    }

    pub fn run(&mut self) {
        let mut event_pump = self.sdl_context.event_pump().unwrap();

        'main_loop: loop {
            let mut events_to_dispatch: Vec<sdl2::event::Event> = vec![];

            self.canvas.set_draw_color(Color::RGB(0, 0, 0));
            self.canvas.clear();

            for event in event_pump.poll_iter() {
                match event {
                    Event::Quit { .. } |
                    Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                        break 'main_loop
                    },
                    // Event::KeyDown { keycode: Some(Keycode::K), ..} => {
                    //     // println!("keydonw k: {}", timestamp);
                    //     println!("keydown k from main");
                    // },
                    // Event::KeyDown { keycode: Some(Keycode::J), ..} => {
                    //     // println!("keydown j: {}", timestamp);
                    // },
                    // Event::Window { win_event: WindowEvent::TakeFocus, ..} => {
                    //     // println!("focus taken");
                    // },
                    // Event::Window { win_event: WindowEvent::FocusGained, ..} => {
                    //     // println!("focus gained");
                    // },
                    // Event::MouseButtonDown { x, y, .. } => {
                    //     if last_x == 0 && last_y == 0 {
                    //         last_x = x;
                    //         last_y = y;
                    //     } else {
                    //         let line = Line {
                    //             a: Point::new(last_x, last_y),
                    //             b: Point::new(x, y),
                    //         };
                    //         last_x = x;
                    //         last_y = y;
                    //         lines.push(line);
                    //     }
                    // },
                    _ => {
                        // println!("event: {:?}", event);
                        events_to_dispatch.push(event);
                    }
                }
            }


            for entity in self.entities.iter_mut() {
                entity.before_update(self.canvas, &events_to_dispatch);
            }

            self.map.render(self.canvas);

            for entity in self.entities.iter_mut() { 
                entity.update(self.canvas, &events_to_dispatch);
            }

            self.canvas.present();

            for entity in self.entities.iter_mut() {
                entity.after_update(self.canvas, &events_to_dispatch);
            }

            ::std::thread::sleep(Duration::new(0, 1_000_000_000u32 / 60));
        }
    }
}
