pub const WINDOW_DIMS: (i32, i32) = (800, 600);
pub const GRID: (i32, i32) = (80, 60);
pub const CELL_SIZE: (i32, i32) = (WINDOW_DIMS.0 / GRID.0, WINDOW_DIMS.1 / GRID.1);
