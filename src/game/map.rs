use std::fmt;
use sdl2::render::{Canvas};
use sdl2::video::{Window};
use sdl2::pixels::Color;
use sdl2::rect::{Point, Rect};
use super::{Tile};
use super::config;

pub struct Map {
    pub w: i32,
    pub h: i32,
    pub tiles: Vec<Tile>,
}

impl Map {
    pub fn new(w: i32, h: i32) -> Map {
        let mut tiles:Vec<Tile> = Vec::new();
        for _ in 0..(w * h) {
            tiles.push(Tile::Empty);
        }
        Map { w, h, tiles }
    }

    pub fn get_tile(&self, x: i32, y: i32) -> &Tile {
        let idx = y * self.w + x;
        &self.tiles[idx as usize]
    }

    pub fn set_tile(&mut self, x: i32, y: i32, tile: Tile) {
        let idx = y * self.w + x;
        self.tiles[idx as usize] = tile;
    }

    pub fn generate(&mut self) {
        for i in 0..self.w {
            for j in 0..self.h {
                if i == 0 || j == 0 || i == self.w - 1 || j == self.h - 1 {
                    self.set_tile(i, j, Tile::Wall);
                }
            }
        }

        self.set_tile(self.w / 2, self.h / 2, Tile::Stairs);

        self.set_tile(self.w / 3, self.h / 3, Tile::Door);
    }

    pub fn render(&self, canvas: &mut Canvas<Window>) {
        let cell_width = config::WINDOW_DIMS.0 / config::GRID.0 as i32;
        let cell_height = config::WINDOW_DIMS.1 / config::GRID.1 as i32;
        canvas.set_draw_color(Color::RGB(128, 128, 128));

        // draw cols
        let mut i = 0;
        while i < config::WINDOW_DIMS.0 {
            let p1 = Point::new(i, 0);
            let p2 = Point::new(i, config::WINDOW_DIMS.1);
            canvas.draw_line(p1, p2).unwrap();
            i += cell_width;
        }

        // draw rows
        let mut j = 0;
        while j < config::WINDOW_DIMS.1 {
            let p1 = Point::new(0, j);
            let p2 = Point::new(config::WINDOW_DIMS.0, j);
            canvas.draw_line(p1, p2).unwrap();
            j += cell_height;
        }

        for i in 0..config::GRID.0 {
            for j in 0..config::GRID.1 {
                match self.get_tile(i, j) {
                    Tile::Wall => {
                        // red
                        canvas.set_draw_color(Color::RGB(255, 0, 0));
                        let rect = Rect::new(i * 10, j * 10, 10, 10);
                        canvas.fill_rect(rect).unwrap();
                        canvas.draw_rect(rect).unwrap();
                    },
                    Tile::Stairs => {
                        // green 
                        canvas.set_draw_color(Color::RGB(0, 255, 0));
                        let rect = Rect::new(i * 10, j * 10, 10, 10);
                        canvas.fill_rect(rect).unwrap();
                        canvas.draw_rect(rect).unwrap();
                    },
                    Tile::Door => {
                        // blue
                        canvas.set_draw_color(Color::RGB(0, 0, 255));
                        let rect = Rect::new(i * 10, j * 10, 10, 10);
                        canvas.fill_rect(rect).unwrap();
                        canvas.draw_rect(rect).unwrap();
                    },
                    _ => {},
                }
            }
        }
    }
}

impl fmt::Display for Map {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut res = String::from("");
        for i in 0..self.w {
            for j in 0..self.h {
                res.push_str(
                    String::from(
                        format!("({},{})={:?}", i, j, self.get_tile(i, j))
                    )
                    .as_ref()
                );
            }
            res.push_str(
                String::from(
                    format!("\n")
                )
                .as_ref()
            );
        }
        write!(f, "{}", res)
    }
}
