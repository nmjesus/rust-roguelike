#[derive(Debug)]
pub enum Tile {
    Empty,
    Wall,
    Stairs,
    Door,
}
