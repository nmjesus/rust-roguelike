use sdl2::render::{Canvas};

pub trait Entity {
    fn before_update(&mut self, canvas: &mut Canvas<sdl2::video::Window>, events: &Vec<sdl2::event::Event>);
    fn update(&mut self, canvas: &mut Canvas<sdl2::video::Window>, events: &Vec<sdl2::event::Event>);
    fn after_update(&mut self, canvas: &mut Canvas<sdl2::video::Window>, events: &Vec<sdl2::event::Event>);
}
